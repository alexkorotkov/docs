#!/bin/bash

HEAD_FOR_REMOVING="</head>"
HEAD='<link rel="stylesheet" type="text/css" href="header.css"><link rel="stylesheet" type="text/css" href="footer.css"><link rel="stylesheet" type="text/css" href="mobile_header.css"><link rel="stylesheet" type="text/css" href="section_separator.css"><script src="js/jquery.js"></script><script>$(function(){$("#doc_header").load("./docs/doc_header.html");$("#doc_footer").load("./docs/doc_footer.html");});</script></head>'

HEADER_FOR_REMOVING='<div class="fixedHeaderContainer"><div class="headerWrapper wrapper"><header><a href="/"><img class="logo" src="./img/favicon.ico" alt="MakeML"/><h2 class="headerTitleWithLogo">MakeML</h2></a><div class="navigationWrapper navigationSlider"><nav class="slidingNav"><ul class="nav-site nav-site-internal"></ul></nav></div></header></div></div>'
HEADER='<div id="doc_header"></div>'

FOOTER_FOR_REMOVING='<footer class="nav-footer" id="footer"><section class="sitemap"></section><section class="copyright">Copyright © 2020 K&amp;K ML Technologies, Inc.</section></footer></div>'
FOOTER='</div><div id="doc_footer"></div>'

files_array=(nails_segmentation_tutorial.html postage_stamps_tutorial.html potato_scales_tutorial.html soccer_ball_tutorial.html candle_fire_tutorial.html)
for file in "${files_array[@]}"
do
  echo $file
  sed -i '' "s~$HEAD_FOR_REMOVING~$HEAD~g" "$file"
  sed -i '' "s~$HEADER_FOR_REMOVING~$HEADER~g" "$file"
  sed -i '' "s~$FOOTER_FOR_REMOVING~$FOOTER~g" "$file"
done
